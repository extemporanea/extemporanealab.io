---
# Feel free to add content and custom Front Matter to this file.
# To modify the layout, see https://jekyllrb.com/docs/themes/#overriding-theme-defaults

layout: home
---

**eXtemporanea**  is a community of people interested in the relationship between science and society.

Our main activities are:


- [**Scienceground**]([Festivaletteratura](https://2020.festivaletteratura.it/scienceground/)): a series of events and debates hosted by [Festivaletteratura](https://www.festivaletteratura.it) in Mantua;

- The blog **scienceground.it** . &rarr; [scienceground.it](https://scienceground.it/en)

<!-- - newsletter -->

<!-- - [chi siamo]({{ site.baseurl }}{% link who.md %}) -->


![demo](assets/imgs/demo.png)
