# Meta

Progetto per raccogliere tutto ciò che riguarda **eXtemporanea** in quanto tale.

## Wiki

Un Wiki interno è esplorabile [qui](../../wikis/Home).

## Sito web eXtemporanea

Il repository contiene i documenti per il sito https://extemporanea.gitlab.io .

Il sito usa [jekyll](https://jekyllrb.com) per generare le pagine. Questo vuole dire che

- per aggiungere contenuti al sito è sufficiente modificare i documenti markdown `.md`, quali
  ```
  - index.md
  - about.md
  - what.md
  - who.md
  ```

  Questo può essere fatto direttamente con l'editor di Gitlab.

- per vedere i risultati delle modifiche sul sito, occorre attendere che Gitlab ricompili il sito. Questo normalmente prende un po' di tempo (qualche minuto). Per monitorare l'avanzamento, cliccare su CI/CDI &rarr; Pipelines. 
