---
layout: page
title: What
permalink: /what/
---


## What do we do?


### scienceground

Scienceground is an interdisciplinary project, organized by eXtemporanea every year in the second week of September in Mantova, Italy, in the context of Festivaletteratura, one of the most important literary festivals in Europe. The project involves various activities: workshops with children and kids, collective reading groups, training on how to read a scientific paper, podcasts, interviews, "scientific walks" in hybrid landscapes. Most importantly, Scienceground is an open space where scientists can meet with the public and have informal conversations.


### Libere letture

We regularly organise book clubs on selected titles that allow us to explore new areas.

Recently we organised the following _Libere letture_ :
- _Against Method_, a book club on the homonymous essay by P. Feyerabend on a sceptic look at the scientific method. [&rarr; Read more (Italian)](https://extemporanea.gitlab.io/against-method)
- _Linguacce_, a parallel reading of texts from opposing schools in linguistics.  [&rarr; Read more (Italian)](https://extemporanea.gitlab.io/linguacce)
